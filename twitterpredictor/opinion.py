from twitter_collect.dataframe import *
from textblob import TextBlob

def get_opinion(tweet):

    a=TextBlob(tweet)
    if a.sentiment[0]>-0.1 and a.sentiment[0]<0.1 and a.sentiment[1]>-0.1 and a.sentiment[1]<0.1:
        return 0
    elif a.sentiment[0]>=0.1:
        return 1
    else:
        return -1


if __name__ == '__main__':

    #获取dataframe:data
    from twitter_collect.collect_candidate_actuality_tweets import *
    twitter_api = twitter_setup()
    num_candidate = 1
    file_path = '../CandidateData'
    candidate_query = get_candidate_queries(num_candidate, file_path)
    tweet_result = get_tweets_from_candidates_search_queries(candidate_query, twitter_api)
    data=transform_to_datafram(tweet_result[1])
    print(data)

    pos_tweets=[]
    neu_tweets = []
    neg_tweets = []

    for tweet in data['text_content']:
        if get_opinion(tweet)==1:
            pos_tweets.append(tweet)
        elif get_opinion(tweet)==0:
            neu_tweets.append(tweet)
        else:
            neg_tweets.append(tweet)

    print("Percentage of positive tweets: {}%".format(len(pos_tweets) * 100 / len(data['text_content'])))
    print("Percentage of neutral tweets: {}%".format(len(neu_tweets) * 100 / len(data['text_content'])))
    print("Percentage de negative tweets: {}%".format(len(neg_tweets) * 100 / len(data['text_content'])))

