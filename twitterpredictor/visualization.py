from twitter_collect.dataframe import *
import pandas as pd
from matplotlib import pyplot as plt

if __name__ == '__main__':

    #获取dataframe:data
    from twitter_collect.collect_candidate_actuality_tweets import *
    twitter_api = twitter_setup()
    num_candidate = 1
    file_path = '../CandidateData'
    candidate_query = get_candidate_queries(num_candidate, file_path)
    tweet_result = get_tweets_from_candidates_search_queries(candidate_query, twitter_api)
    data=transform_to_datafram(tweet_result[1])
   # print(data)


    # 转发量和点赞量随时间的变化
    tfav = pd.Series(data=data['Likes'].values, index=data['Date'])
    tret = pd.Series(data=data['RTs'].values, index=data['Date'])

    # Likes vs retweets visualization:
    tfav.plot(figsize=(16, 4), label="Likes", legend=True)
    tret.plot(figsize=(16, 4), label="Retweets", legend=True)

    plt.show()
    