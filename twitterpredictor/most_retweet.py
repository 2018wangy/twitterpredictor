from twitter_collect.dataframe import *

if __name__ == '__main__':

    #获取dataframe:data
    from twitter_collect.collect_candidate_actuality_tweets import *
    twitter_api = twitter_setup()
    num_candidate = 1
    file_path = '../CandidateData'
    candidate_query = get_candidate_queries(num_candidate, file_path)
    tweet_result = get_tweets_from_candidates_search_queries(candidate_query, twitter_api)
    data=transform_to_datafram(tweet_result[1])
   # print(data)




    #获取转发量最多的tweet
    rt_max = np.max(data['RTs'])
    rt = data[data.RTs == rt_max].index[0]
    #print(rt)
    # Max RTs:
    print("The tweet with more retweets is: \n{}".format(data['text_content'][rt]))
    print("Number of retweets: {}".format(rt_max))
    print("{} characters.\n".format(data['len'][rt]))

