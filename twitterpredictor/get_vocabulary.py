from textblob import TextBlob
from textblob import Word
from twitter_collect.collect1 import collect


def get_vocabulary(tweets):
    '''
    :param tweets: emsemble of tweets
    :return: a list of useful vocabularies
    '''
    total = ''
    vocabulary = []

    for tweet in tweets:
        tweet_text = tweet.text
        total += tweet_text

    for tweet in tweets:

        tweet_text = tweet.text
        wiki = TextBlob(tweet_text)
        word_list = wiki.words  # class 'textblob.blob.WordList'

        for w in word_list:
            if ('/' not in str(w)) and ('"' not in str(w)):
                w = Word(w.lower())
                w = Word(w.lemmatize())
                w = Word(w.correct())
                w = Word(w.singularize())
                if TextBlob(str(w)).tags[0][1] == 'VBD' or 'VBG':
                    w = Word(w.lemmatize("v"))
              #  print(w)
                if TextBlob(total).words.count(str(w)) < 3:
                    if str(w) not in vocabulary:
                        vocabulary.append(str(w))

    return vocabulary

if __name__ == '__main__':
    tweets = collect("DonaldTrump", "english")
    res = get_vocabulary(tweets)
    for e in res :
        print(e)



