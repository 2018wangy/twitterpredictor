import pandas as pd

def transform_to_dict(tweet):   #输入tweepy类型的一条tweet
    """
    This function input a single tweet and extract some content from it
    :param tweet:a single tweet
    :return:    a dictionary contains some contents extracted from the given tweet
    """
    return {"text_content":str(tweet.text), #正文内容
            "len":len(tweet.text),          #正文长度
            "Likes":tweet.favorite_count, #点赞数量
            "RTs":tweet.retweet_count, #转发数量
            "Date":tweet.created_at   #发表时间
            }

def transform_to_datafram(tweets):
    """

    :param tweets:tweets集合
    :return:包含上述五个参数的dataframe
    """
    dict_list = []
    for tweet in tweets:
        dict_list.append(transform_to_dict(tweet))
    #print(dict_list)
    data_frame = pd.DataFrame(dict_list)
    return data_frame

if __name__ == '__main__':

    #获取dataframe:data
    from twitter_collect.collect_candidate_actuality_tweets import *
    twitter_api = twitter_setup()
    num_candidate = 1
    file_path = '../CandidateData'
    candidate_query = get_candidate_queries(num_candidate, file_path)
    tweet_result = get_tweets_from_candidates_search_queries(candidate_query, twitter_api)
    data=transform_to_datafram(tweet_result[1])

    print(data)




