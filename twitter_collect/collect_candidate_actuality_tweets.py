from twitter_collect.twitter_connection_setup import *
from twitter_collect.collect1 import *
from tweepy.error import RateLimitError

def get_tweets_from_candidates_search_queries(queries, twitter_api):
    '''
        Now we'll do a terme based seach for each query and hashtag and return the queries
        :param: queries：a list of keywords and/or hastags
        :param:twitter_api: set_up函数返回的接口
        :return:tweet集组成的list,第i个元素为queries中第i个元素搜索得到了tweet集合
    '''

    results = []
    try:
        for query in queries:
            query_result = twitter_api.search(query, language='french', rpp=100)
            results.append(query_result)
    except RateLimitError:
        print('An error occurred during the query, please check the API')

    return results

if __name__ == '__main__':
    twitter_api = twitter_setup()
    num_candidate = 1
    file_path = '../CandidateData'
    candidate_query = get_candidate_queries(num_candidate, file_path)
    tweet_result = get_tweets_from_candidates_search_queries(candidate_query, twitter_api)
    for tweet in tweet_result[0]:   #the number means the i th element of the list of keywords+hashtags
       print('-> {}\n'.format(tweet.text))