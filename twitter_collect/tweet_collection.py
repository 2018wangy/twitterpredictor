from twitter_collect.twitter_connection_setup import twitter_setup  #初始化API端口
from twitter_collect.collect_candidate_actuality_tweets import get_candidate_queries    #询问关键词query
from twitter_collect.collect_candidate_actuality_tweets import  get_tweets_from_candidates_search_queries   #获取目标的tweet
import json
import tweepy

def structure_tweets(tweet):
    if isinstance(tweet, tweepy.models.Status): #检验是否为tweepy类型
        return {"text": str(tweet.text),        #编辑存储类容以及变量名称
                "id": tweet.id,
                "retweeted": tweet.retweeted,
                "retweet_count": tweet.retweet_count,
                "favorite_count": tweet.favorite_count,
                "created_at": tweet.created_at.isoformat(),
                "followers_count": tweet.user.followers_count,
                "description": str(tweet.user.description),
                "name": tweet.user.name}
    raise TypeError(repr(tweet) + " Not the same object type")  #错误处理

def store_tweets(queries,filename):
    '''

    :param queries: tweet集的集合
    :param filename: 要存入数据的文件名（文件夹建好，文件自动生成）
    :return: 把queries中所有tweet变成json形式并写入指定文件中
    '''
    json_to_dump = []
    for query in queries:   #对于每一个关键词或标签
        #print(query.__class__)
        for tweet in query: #对于每一个关键词或标签下的每一条tweet
            json_to_dump.append(structure_tweets(tweet))
    with open(filename, 'w', encoding='utf-8') as f:
        json.dump(json_to_dump,f,indent=4)


if __name__ == '__main__':
    num_condidat = 2
    queries = get_candidate_queries(num_condidat,'../CandidateData')    #获取1号参选人的query
    api_instance = twitter_setup()  #建立API接口
    queries_results = get_tweets_from_candidates_search_queries(queries,api_instance)   #获取query所对应的tweets
    output_filename = '../jsonDump/tweets_candidate_' + str(num_condidat) + '.json'     #编辑存储路径
    store_tweets(queries_results,output_filename)   #存储抓取到的tweets