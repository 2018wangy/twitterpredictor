import tweepy
from twitter_collect.twitter_connection_setup import *

#l'API Search: print the tweets of a search
def collect(keywords,langue):
    '''
    :return: 搜索关键词"Emmanuel Macron"得到的tweets
    '''
    connexion = twitter_setup()
    tweets = connexion.search(keywords,language=langue,rpp=100)
    #for tweet in tweets:
      #  print(tweet.text)
    return tweets
#print(collect())

#l'API Users: print the tweets of a given user
def collect_by_user(user_id):
    '''

    :param user_id:
    :return: 该user的tweets
    '''
    connexion = twitter_setup()
    statuses = connexion.user_timeline(id = user_id, count = 200)
    #for status in statuses:
      #  print(status.text)
    return statuses
#print(collect_by_user(1976143068))

#l'API Streaming: get the real time tweets of a search
from tweepy.streaming import StreamListener

class StdOutListener(StreamListener):

    def on_data(self, data):
        print(data)
        return True

    def on_error(self, status):
        if  str(status) == "420":
            print(status)
            print("You exceed a limited number of attempts to connect to the streaming API")
            return False
        else:
            return True

def collect_by_streaming():

    connexion = twitter_setup()
    listener = StdOutListener()
    stream=tweepy.Stream(auth = connexion.auth, listener=listener)
    stream.filter(track=['Emmanuel Macron'])

#get the list of keywords and hashtags of a given candidat
def get_candidate_queries(num_candidate, file_path):
    """
    Generate and return a list of string queries for the search Twitter API from the file file_path_num_candidate.txt
    :param num_candidate: the number of the candidate
    :param file_path: the path to the keyword and hashtags files
    :return: (list) a list of string queries that can be done to the search API independently
    """

    file_name_keywords='\\keywords_candidate_'+str(num_candidate)+'.txt'
    file_name_hashtags='\\hashtags_candidate_'+str(num_candidate)+'.txt'

    path_keywords = file_path+file_name_keywords
    path_hashtags = file_path + file_name_hashtags

    try:
        with open(path_keywords, 'r') as file :
            keywords = file.readlines()
        #print('The loaded keywords : {}'.format(keywords))
        with open(path_hashtags,'r') as file2:
            hashtags = file2.readlines()
        #print('The loaded hashtags : {}'.format(hashtags))
        return keywords+hashtags
    except IOError:
        print('The file cannot be opened, please check the given path / candidate number')

