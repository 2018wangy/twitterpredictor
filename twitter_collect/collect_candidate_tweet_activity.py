from twitter_collect.twitter_connection_setup import *
def get_retweets_of_candidate(candidate_username):
    """
    Count the number of retweets for the most recent tweet of a certain candidate
    :param candidate_username: the screen name of a candidate's twitter
    :return:该user的非转发tweets的内容和转发数组成的dictionary
    """
    twitter_api = twitter_setup()   #setup an API
    tweets  = twitter_api.user_timeline(screen_name=candidate_username, count=200)   #retrieve the real time tweets
    retweets_per_tweet = {}
    for status in tweets:
        # Only for original tweets, excluding the retweets
        if (not status.retweeted) or ('RT @' not in status.text): #I should test whether retweeted is 0 or 1
            retweets_per_tweet[status.text] = status.retweet_count
        return retweets_per_tweet

############## Testing #################
if __name__ == "__main__": # If we execute this file
    username = 'EmmanuelMacron'
    retweets_per_tweet = get_retweets_of_candidate(username)
    for tweet, nb_rt in retweets_per_tweet.items():
        print('{}, the number of retweets are --> {}'.format(tweet, nb_rt))