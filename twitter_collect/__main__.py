#获得搜索结果推文、实施推文转发数
from twitter_collect.twitter_connection_setup import *
from twitter_collect.collect_candidate_actuality_tweets import *
from twitter_collect.collect_candidate_tweet_activity import *

if __name__ == '__main__':
    twitter_api = twitter_setup()
    num_candidate = 1
    file_path = '../CandidateData'
    candidate_query = get_candidate_queries(num_candidate, file_path)
    tweet_result = get_tweets_from_candidates_search_queries(candidate_query, twitter_api)
    for tweet in tweet_result[1]:   #the number means the i th keyword
       print('-> {}\n'.format(tweet.text))
    username = 'EmmanuelMacron'
    retweets_per_tweet = get_retweets_of_candidate(username)
    for tweet, nb_rt in retweets_per_tweet.items():
        print('{}, the number of retweets are --> {}'.format(tweet, nb_rt))